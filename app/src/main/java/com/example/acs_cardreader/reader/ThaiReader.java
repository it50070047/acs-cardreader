package com.example.acs_cardreader.reader;

import android.util.Log;

import com.acs.smartcard.Reader;
import com.acs.smartcard.ReaderException;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;

public class ThaiReader {
    byte[] select = {(byte) 0x00, (byte) 0xA4, (byte) 0x04, (byte) 0x00, (byte) 0x08, (byte) 0xA0, (byte) 0x00,
            (byte) 0x00, (byte) 0x00, (byte) 0x54, (byte) 0x48, (byte) 0x00, (byte) 0x01};

    byte[] cid = {(byte) 0x80, (byte) 0xB0, (byte) 0x00, (byte) 0x04, (byte) 0x02, (byte) 0x00, (byte) 0x0D};
    byte[] cidGetData = {(byte) 0x00, (byte) 0xC0, (byte) 0x00, (byte) 0x00, (byte) 0x0D};
    byte[] nameTH = {(byte) 0x80, (byte) 0xB0, (byte) 0x00, (byte) 0x11, (byte) 0x02, (byte) 0x00, (byte) 0x64};
    byte[] nameTHGetData = {(byte) 0x00, (byte) 0xC0, (byte) 0x00, (byte) 0x00, (byte) 0x64};
    byte[] nameEN = {(byte) 0x80, (byte) 0xB0, (byte) 0x00, (byte) 0x75, (byte) 0x02, (byte) 0x00, (byte) 0x64};
    byte[] nameENGetData = {(byte) 0x00, (byte) 0xC0, (byte) 0x00, (byte) 0x00, (byte) 0x64};
    byte[] birthdate = {(byte) 0x80, (byte) 0xB0, (byte) 0x00, (byte) 0xD9, (byte) 0x02, (byte) 0x00, (byte) 0x08};
    byte[] birthdateGetData = {(byte) 0x00, (byte) 0xC0, (byte) 0x00, (byte) 0x00, (byte) 0x08};
    byte[] gender = {(byte) 0x80, (byte) 0xB0, (byte) 0x00, (byte) 0xE1, (byte) 0x02, (byte) 0x00, (byte) 0x01};
    byte[] genderGetData = {(byte) 0x00, (byte) 0xC0, (byte) 0x00, (byte) 0x00, (byte) 0x01};
    byte[] address = {(byte) 0x80, (byte) 0xB0, (byte) 0x15, (byte) 0x79, (byte) 0x02, (byte) 0x00, (byte) 0x64};
    byte[] addressGetData = {(byte) 0x00, (byte) 0xC0, (byte) 0x00, (byte) 0x00, (byte) 0x64};
    byte[] cardIssuer = {(byte) 0x80, (byte) 0xB0, (byte) 0x00, (byte) 0xF6, (byte) 0x02, (byte) 0x00, (byte) 0x64};
    byte[] cardIssuerGetData = {(byte) 0x00, (byte) 0xC0, (byte) 0x00, (byte) 0x00, (byte) 0x64};
    byte[] issueDate = {(byte) 0x80, (byte) 0xB0, (byte) 0x01, (byte) 0x67, (byte) 0x02, (byte) 0x00, (byte) 0x08};
    byte[] issueDateGetData = {(byte) 0x00, (byte) 0xC0, (byte) 0x00, (byte) 0x00, (byte) 0x08};
    byte[] expireDate = {(byte) 0x80, (byte) 0xB0, (byte) 0x01, (byte) 0x6F, (byte) 0x02, (byte) 0x00, (byte) 0x08};
    byte[] expireDateGetData = {(byte) 0x00, (byte) 0xC0, (byte) 0x00, (byte) 0x00, (byte) 0x08};
    byte[][] photo = {
            {(byte) 0x80, (byte) 0xB0, (byte) 0x01, (byte) 0x7B, (byte) 0x02, (byte) 0x00, (byte) 0xFF},
            {(byte) 0x80, (byte) 0xB0, (byte) 0x02, (byte) 0x7A, (byte) 0x02, (byte) 0x00, (byte) 0xFF},
            {(byte) 0x80, (byte) 0xB0, (byte) 0x03, (byte) 0x79, (byte) 0x02, (byte) 0x00, (byte) 0xFF},
            {(byte) 0x80, (byte) 0xB0, (byte) 0x04, (byte) 0x78, (byte) 0x02, (byte) 0x00, (byte) 0xFF},
            {(byte) 0x80, (byte) 0xB0, (byte) 0x05, (byte) 0x77, (byte) 0x02, (byte) 0x00, (byte) 0xFF},
            {(byte) 0x80, (byte) 0xB0, (byte) 0x06, (byte) 0x76, (byte) 0x02, (byte) 0x00, (byte) 0xFF},
            {(byte) 0x80, (byte) 0xB0, (byte) 0x07, (byte) 0x75, (byte) 0x02, (byte) 0x00, (byte) 0xFF},
            {(byte) 0x80, (byte) 0xB0, (byte) 0x08, (byte) 0x74, (byte) 0x02, (byte) 0x00, (byte) 0xFF},
            {(byte) 0x80, (byte) 0xB0, (byte) 0x09, (byte) 0x73, (byte) 0x02, (byte) 0x00, (byte) 0xFF},
            {(byte) 0x80, (byte) 0xB0, (byte) 0x0A, (byte) 0x72, (byte) 0x02, (byte) 0x00, (byte) 0xFF},
            {(byte) 0x80, (byte) 0xB0, (byte) 0x0B, (byte) 0x71, (byte) 0x02, (byte) 0x00, (byte) 0xFF},
            {(byte) 0x80, (byte) 0xB0, (byte) 0x0C, (byte) 0x70, (byte) 0x02, (byte) 0x00, (byte) 0xFF},
            {(byte) 0x80, (byte) 0xB0, (byte) 0x0D, (byte) 0x6F, (byte) 0x02, (byte) 0x00, (byte) 0xFF},
            {(byte) 0x80, (byte) 0xB0, (byte) 0x0E, (byte) 0x6E, (byte) 0x02, (byte) 0x00, (byte) 0xFF},
            {(byte) 0x80, (byte) 0xB0, (byte) 0x0F, (byte) 0x6D, (byte) 0x02, (byte) 0x00, (byte) 0xFF},
            {(byte) 0x80, (byte) 0xB0, (byte) 0x10, (byte) 0x6C, (byte) 0x02, (byte) 0x00, (byte) 0xFF},
            {(byte) 0x80, (byte) 0xB0, (byte) 0x11, (byte) 0x6B, (byte) 0x02, (byte) 0x00, (byte) 0xFF},
            {(byte) 0x80, (byte) 0xB0, (byte) 0x12, (byte) 0x6A, (byte) 0x02, (byte) 0x00, (byte) 0xFF},
            {(byte) 0x80, (byte) 0xB0, (byte) 0x13, (byte) 0x69, (byte) 0x02, (byte) 0x00, (byte) 0xFF},
            {(byte) 0x80, (byte) 0xB0, (byte) 0x14, (byte) 0x68, (byte) 0x02, (byte) 0x00, (byte) 0xFF},
    };

    byte[] photoGetData = {(byte) 0x00, (byte) 0xC0, (byte) 0x00, (byte) 0x00, (byte) 0xFF};

    byte[] religion = {(byte) 0x80, (byte) 0xB0, (byte) 0x01, (byte) 0x77, (byte) 0x02, (byte) 0x00, (byte) 0x02};
    byte[] religionGetData = {(byte) 0x00, (byte) 0xC0, (byte) 0x00, (byte) 0x00, (byte) 0x02};

    byte[] referenceNo = {(byte) 0x80, (byte) 0xB0, (byte) 0x16, (byte) 0x19, (byte) 0x02, (byte) 0x00, (byte) 0x0E};
    byte[] referenceNoGetData = {(byte) 0x00, (byte) 0xC0, (byte) 0x00, (byte) 0x00, (byte) 0x0E};

    byte[] checkChip = {(byte) 0x00, (byte) 0xA4, (byte) 0x04, (byte) 0x00, (byte) 0x08, (byte) 0xA0,
            (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x84, (byte) 0x06, (byte) 0x00, (byte) 0x02};

    byte[] chip = {(byte) 0x80, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x07};
    byte[] chipGetData = {(byte) 0x00, (byte) 0xc0, (byte) 0x00, (byte) 0x00, (byte) 0x10};

    String[] allDataList = {
            "cid",
            "nameTH",
            "nameEN",
            "birthdate",
            "gender",
            "address",
            "cardIssuer",
            "issueDate",
            "expireDate",
            "photo",
            "religion",
            "referenceNo",
            "chip"
    };

    public HashMap<String, Object> readAll(Reader r) throws ReaderException {
        return readSpecific(r, allDataList);
    }

    public HashMap<String, Object> readSpecific(Reader r, String[] reqList) throws ReaderException {
        HashMap<String, Object> response = new HashMap<>();
        byte[] respArray = new byte[500];
        int responseLength;
        int slotNum = 0;
        resetCard(r);
        setProtocol(r);
        select(r);
        if (Arrays.asList(reqList).contains("cid")) {
            r.transmit(slotNum, cid, cid.length, respArray, respArray.length);
            responseLength = r.transmit(slotNum, cidGetData, cidGetData.length, respArray, respArray.length);
            String hexString = byteArrayToHexString(respArray, 0, responseLength);
            if (hexString != null) {
                response.put("cid", hexString);
            }
        }
        if (Arrays.asList(reqList).contains("nameTH")) {
            r.transmit(slotNum, nameTH, nameTH.length, respArray, respArray.length);
            responseLength = r.transmit(slotNum, nameTHGetData, nameTHGetData.length, respArray, respArray.length);
            String hexString = byteArrayToHexString(respArray, 0, responseLength);
            if (hexString != null) {
                response.put("nameTH", hexString);
            }
        }
        if (Arrays.asList(reqList).contains("nameEN")) {
            r.transmit(slotNum, nameEN, nameEN.length, respArray, respArray.length);
            responseLength = r.transmit(slotNum, nameENGetData, nameENGetData.length, respArray, respArray.length);
            String hexString = byteArrayToHexString(respArray, 0, responseLength);
            if (hexString != null) {
                response.put("nameEN", hexString);
            }
        }
        if (Arrays.asList(reqList).contains("birthdate")) {
            r.transmit(slotNum, birthdate, birthdate.length, respArray, respArray.length);
            responseLength = r.transmit(slotNum, birthdateGetData, birthdateGetData.length, respArray, respArray.length);
            String hexString = byteArrayToHexString(respArray, 0, responseLength);
            if (hexString != null) {
                response.put("birthdate", hexString);
            }
        }
        if (Arrays.asList(reqList).contains("gender")) {
            r.transmit(slotNum, gender, gender.length, respArray, respArray.length);
            responseLength = r.transmit(slotNum, genderGetData, genderGetData.length, respArray, respArray.length);
            String hexString = byteArrayToHexString(respArray, 0, responseLength);
            if (hexString != null) {
                response.put("gender", hexString);
            }
        }
        if (Arrays.asList(reqList).contains("address")) {
            r.transmit(slotNum, address, address.length, respArray, respArray.length);
            responseLength = r.transmit(slotNum, addressGetData, addressGetData.length, respArray, respArray.length);
            String hexString = byteArrayToHexString(respArray, 0, responseLength);
            if (hexString != null) {
                response.put("address", hexString);
            }
        }
        if (Arrays.asList(reqList).contains("cardIssuer")) {
            r.transmit(slotNum, cardIssuer, cardIssuer.length, respArray, respArray.length);
            responseLength = r.transmit(slotNum, cardIssuerGetData, cardIssuerGetData.length, respArray, respArray.length);
            String hexString = byteArrayToHexString(respArray, 0, responseLength);
            if (hexString != null) {
                response.put("cardIssuer", hexString);
            }
        }
        if (Arrays.asList(reqList).contains("issueDate")) {
            r.transmit(slotNum, issueDate, issueDate.length, respArray, respArray.length);
            responseLength = r.transmit(slotNum, issueDateGetData, issueDateGetData.length, respArray, respArray.length);
            String hexString = byteArrayToHexString(respArray, 0, responseLength);
            if (hexString != null) {
                response.put("issueDate", hexString);
            }
        }
        if (Arrays.asList(reqList).contains("expireDate")) {
            r.transmit(slotNum, expireDate, expireDate.length, respArray, respArray.length);
            responseLength = r.transmit(slotNum, expireDateGetData, expireDateGetData.length, respArray, respArray.length);
            String hexString = byteArrayToHexString(respArray, 0, responseLength);
            if (hexString != null) {
                response.put("expireDate", hexString);
            }
        }
        if (Arrays.asList(reqList).contains("photo")) {
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            for (int i = 0; i < photo.length; i++) {
                r.transmit(slotNum, photo[i], photo[i].length, respArray, respArray.length);
                responseLength = r.transmit(slotNum, photoGetData, photoGetData.length, respArray, respArray.length);
                buffer.write(respArray, 0, responseLength - 2);
            }
            byte[] photoBuffer = buffer.toByteArray();
            response.put("photo", photoBuffer);
        }
        if (Arrays.asList(reqList).contains("religion")) {
            r.transmit(slotNum, religion, religion.length, respArray, respArray.length);
            responseLength = r.transmit(slotNum, religionGetData, religionGetData.length, respArray, respArray.length);
            String hexString = byteArrayToHexString(respArray, 0, responseLength);
            if (hexString != null) {
                response.put("religion", hexString);
            }
        }
        if (Arrays.asList(reqList).contains("referenceNo")) {
            r.transmit(slotNum, referenceNo, referenceNo.length, respArray, respArray.length);
            responseLength = r.transmit(slotNum, referenceNoGetData, referenceNoGetData.length, respArray, respArray.length);
            String hexString = byteArrayToHexString(respArray, 0, responseLength);
            if (hexString != null) {
                response.put("referenceNo", hexString);
            }
        }
        if (Arrays.asList(reqList).contains("chip")) {
            r.transmit(0, checkChip, checkChip.length, respArray, respArray.length);
            r.transmit(slotNum, chip, chip.length, respArray, respArray.length);
            responseLength = r.transmit(slotNum, chipGetData, chipGetData.length, respArray, respArray.length);
            String hexString = byteArrayToHexString(respArray, 0, responseLength);
            if (hexString != null) {
                response.put("chip", hexString);
            }
        }
        return response;
    }

    public byte[] resetCard(Reader r) throws ReaderException {
        return r.power(0, Reader.CARD_WARM_RESET);
    }

    public int setProtocol(Reader r) throws ReaderException {
        return r.setProtocol(0, Reader.PROTOCOL_T0);
    }

    public int select(Reader r) throws ReaderException {
        byte[] response = new byte[500];
        r.transmit(0, select, select.length, response, response.length);
        return response.length;
    }

    private String byteArrayToHexString(byte[] input, int index, int responseLength) {
        int length = responseLength;
        if (length + index > input.length) {
            length = input.length - index;
        }
        byte[] selectBytes = new byte[length];
        System.arraycopy(input, index, selectBytes, 0, length - 2);
        return showByteString(selectBytes);
    }

    public String showByteString(byte[] input) {
        StringBuilder output;
        if (input == null) {
            Log.d("TAG", "Empty");
        }
        output = new StringBuilder();
        for (byte b : input) {

            output.append(String.format("%02x", b));
        }
        Log.d("TAG", "Byte String - " + output.toString());

        String result = null;
        try {
            result = new String(input, "TIS620");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            Log.d("TAG", "Cannot parse!");
        }
        return result;
    }
}
