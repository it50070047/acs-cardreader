package com.example.acs_cardreader.smc;

public class ThaiADPU {

    /* SELECT AND RESPONSE */
    public static byte[] SELECT = {(byte) 0x00, (byte) 0xA4, (byte) 0x04, (byte) 0x00, (byte) 0x08};
    public static byte[] THAI_CARD = {(byte) 0xA0, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x54, (byte) 0x48, (byte) 0x00, (byte) 0x01};
    public static byte[] CHIP_CARD_AMD = {(byte) 0xA0, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x84, (byte) 0x06, (byte) 0x00, (byte) 0x02};
    public static byte[] RESPONSE = {(byte) 0x00, (byte) 0xC0, (byte) 0x00, (byte) 0x00};

    /* Command */
    public static byte[] CMD_ID = {(byte) 0x80, (byte) 0xB0, (byte) 0x00, (byte) 0x04, (byte) 0x02, (byte) 0x00, (byte) 0x0D};
    public static byte[] CMD_TH_FULL_NAME = {(byte) 0x80, (byte) 0xB0, (byte) 0x00, (byte) 0x11, (byte) 0x02, (byte) 0x00, (byte) 0x64};
    public static byte[] CMD_EN_FULL_NAME = {(byte) 0x80, (byte) 0xB0, (byte) 0x00, (byte) 0x75, (byte) 0x02, (byte) 0x00, (byte) 0x64};
    public static byte[] CMD_BRITH_DATE = {(byte) 0x80, (byte) 0xB0, (byte) 0x00, (byte) 0xD9, (byte) 0x02, (byte) 0x00, (byte) 0x08};
    public static byte[] CMD_GENDER = {(byte) 0x80, (byte) 0xB0, (byte) 0x00, (byte) 0xE1, (byte) 0x02, (byte) 0x00, (byte) 0x01};
    public static byte[] CMD_ISSUER = {(byte) 0x80, (byte) 0xB0, (byte) 0x00, (byte) 0xF6, (byte) 0x02, (byte) 0x00, (byte) 0x64};
    public static byte[] CMD_ISSUE_DATE = {(byte) 0x80, (byte) 0xB0, (byte) 0x01, (byte) 0x67, (byte) 0x02, (byte) 0x00, (byte) 0x08};
    public static byte[] CMD_EXPIRE_DATE = {(byte) 0x80, (byte) 0xB0, (byte) 0x01, (byte) 0x6F, (byte) 0x02, (byte) 0x00, (byte) 0x08};
    public static byte[] CMD_ADDRESS = {(byte) 0x80, (byte) 0xB0, (byte) 0x15, (byte) 0x79, (byte) 0x02, (byte) 0x00, (byte) 0x64};
    public static byte[] CMD_RELIGION = {(byte) 0x80, (byte) 0xB0, (byte) 0x01, (byte) 0x77, (byte) 0x02, (byte) 0x00, (byte) 0x02};
    public static byte[] CMD_REFERENCE_NO = {(byte) 0x80, (byte) 0xB0, (byte) 0x16, (byte) 0x19, (byte) 0x02, (byte) 0x00, (byte) 0x0E};
    public static byte[] CMD_PHOTO1 = {(byte) 0x80, (byte) 0xB0, (byte) 0x01, (byte) 0x7B, (byte) 0x02, (byte) 0x00, (byte) 0xFF};
    public static byte[] CMD_PHOTO2 = {(byte) 0x80, (byte) 0xB0, (byte) 0x02, (byte) 0x7A, (byte) 0x02, (byte) 0x00, (byte) 0xFF};
    public static byte[] CMD_PHOTO3 = {(byte) 0x80, (byte) 0xB0, (byte) 0x03, (byte) 0x79, (byte) 0x02, (byte) 0x00, (byte) 0xFF};
    public static byte[] CMD_PHOTO4 = {(byte) 0x80, (byte) 0xB0, (byte) 0x04, (byte) 0x78, (byte) 0x02, (byte) 0x00, (byte) 0xFF};
    public static byte[] CMD_PHOTO5 = {(byte) 0x80, (byte) 0xB0, (byte) 0x05, (byte) 0x77, (byte) 0x02, (byte) 0x00, (byte) 0xFF};
    public static byte[] CMD_PHOTO6 = {(byte) 0x80, (byte) 0xB0, (byte) 0x06, (byte) 0x76, (byte) 0x02, (byte) 0x00, (byte) 0xFF};
    public static byte[] CMD_PHOTO7 = {(byte) 0x80, (byte) 0xB0, (byte) 0x07, (byte) 0x75, (byte) 0x02, (byte) 0x00, (byte) 0xFF};
    public static byte[] CMD_PHOTO8 = {(byte) 0x80, (byte) 0xB0, (byte) 0x08, (byte) 0x74, (byte) 0x02, (byte) 0x00, (byte) 0xFF};
    public static byte[] CMD_PHOTO9 = {(byte) 0x80, (byte) 0xB0, (byte) 0x09, (byte) 0x73, (byte) 0x02, (byte) 0x00, (byte) 0xFF};
    public static byte[] CMD_PHOTO10 = {(byte) 0x80, (byte) 0xB0, (byte) 0x0A, (byte) 0x72, (byte) 0x02, (byte) 0x00, (byte) 0xFF};
    public static byte[] CMD_PHOTO11 = {(byte) 0x80, (byte) 0xB0, (byte) 0x0B, (byte) 0x71, (byte) 0x02, (byte) 0x00, (byte) 0xFF};
    public static byte[] CMD_PHOTO12 = {(byte) 0x80, (byte) 0xB0, (byte) 0x0C, (byte) 0x70, (byte) 0x02, (byte) 0x00, (byte) 0xFF};
    public static byte[] CMD_PHOTO13 = {(byte) 0x80, (byte) 0xB0, (byte) 0x0D, (byte) 0x6F, (byte) 0x02, (byte) 0x00, (byte) 0xFF};
    public static byte[] CMD_PHOTO14 = {(byte) 0x80, (byte) 0xB0, (byte) 0x0E, (byte) 0x6E, (byte) 0x02, (byte) 0x00, (byte) 0xFF};
    public static byte[] CMD_PHOTO15 = {(byte) 0x80, (byte) 0xB0, (byte) 0x0F, (byte) 0x6D, (byte) 0x02, (byte) 0x00, (byte) 0xFF};
    public static byte[] CMD_PHOTO16 = {(byte) 0x80, (byte) 0xB0, (byte) 0x10, (byte) 0x6C, (byte) 0x02, (byte) 0x00, (byte) 0xFF};
    public static byte[] CMD_PHOTO17 = {(byte) 0x80, (byte) 0xB0, (byte) 0x11, (byte) 0x6B, (byte) 0x02, (byte) 0x00, (byte) 0xFF};
    public static byte[] CMD_PHOTO18 = {(byte) 0x80, (byte) 0xB0, (byte) 0x12, (byte) 0x6A, (byte) 0x02, (byte) 0x00, (byte) 0xFF};
    public static byte[] CMD_PHOTO19 = {(byte) 0x80, (byte) 0xB0, (byte) 0x13, (byte) 0x69, (byte) 0x02, (byte) 0x00, (byte) 0xFF};
    public static byte[] CMD_PHOTO20 = {(byte) 0x80, (byte) 0xB0, (byte) 0x14, (byte) 0x68, (byte) 0x02, (byte) 0x00, (byte) 0xFF};
    public static byte[] CMD_LASER_CODE = {(byte) 0x80, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x07};

}
