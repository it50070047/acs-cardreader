package com.example.acs_cardreader.smc;

import androidx.annotation.NonNull;

public class Person {
    private String id;
    private String fullNameTH;
    private String prefixNameTH;
    private String firstNameTH;
    private String middleNameTH;
    private String lastNameTH;
    private String fullNameEN;
    private String prefixNameEN;
    private String firstNameEN;
    private String middleNameEN;
    private String lastNameEN;
    private String birthDate;
    private String birthDateFormatted;
    private String gender;
    private String issuer;
    private String issueDate;
    private String issueDateFormatted;
    private String expireDate;
    private String expireDateFormatted;
    private String religion;
    private String referenceNo;
    private String laserCode;
    private byte[] photo;
    private String address;
    private String houseNo;
    private String moo;
    private String soi;
    private String street;
    private String subDistrict;
    private String district;
    private String province;

    @NonNull
    @Override
    public String toString() {
        return "Person[id=" + id + ", fullNameTh=" + fullNameTH + ", fullNameEn=" + fullNameEN
                + ", birthDate=" + birthDate + ", gender=" + gender + ", issuer=" + issuer
                + ", issueDate=" + issueDate + ", expireDate=" + expireDate + ", religion=" + religion
                + ", referenceNo=" + referenceNo + ", laserCode=" + laserCode
                + ", address=" + address + "]";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullNameTH() {
        return fullNameTH;
    }

    public void setFullNameTH(String fullNameTH) {
        this.fullNameTH = fullNameTH;
    }

    public String getPrefixNameTH() {
        return prefixNameTH;
    }

    public void setPrefixNameTH(String prefixNameTH) {
        this.prefixNameTH = prefixNameTH;
    }

    public String getFirstNameTH() {
        return firstNameTH;
    }

    public void setFirstNameTH(String firstNameTH) {
        this.firstNameTH = firstNameTH;
    }

    public String getMiddleNameTH() {
        return middleNameTH;
    }

    public void setMiddleNameTH(String middleNameTH) {
        this.middleNameTH = middleNameTH;
    }

    public String getLastNameTH() {
        return lastNameTH;
    }

    public void setLastNameTH(String lastNameTH) {
        this.lastNameTH = lastNameTH;
    }

    public String getFullNameEN() {
        return fullNameEN;
    }

    public void setFullNameEN(String fullNameEN) {
        this.fullNameEN = fullNameEN;
    }

    public String getPrefixNameEN() {
        return prefixNameEN;
    }

    public void setPrefixNameEN(String prefixNameEN) {
        this.prefixNameEN = prefixNameEN;
    }

    public String getFirstNameEN() {
        return firstNameEN;
    }

    public void setFirstNameEN(String firstNameEN) {
        this.firstNameEN = firstNameEN;
    }

    public String getMiddleNameEN() {
        return middleNameEN;
    }

    public void setMiddleNameEN(String middleNameEN) {
        this.middleNameEN = middleNameEN;
    }

    public String getLastNameEN() {
        return lastNameEN;
    }

    public void setLastNameEN(String lastNameEN) {
        this.lastNameEN = lastNameEN;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthDateFormatted() {
        return birthDateFormatted;
    }

    public void setBirthDateFormatted(String birthDateFormatted) {
        this.birthDateFormatted = birthDateFormatted;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getLaserCode() {
        return laserCode;
    }

    public void setLaserCode(String laserCode) {
        this.laserCode = laserCode;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getMoo() {
        return moo;
    }

    public void setMoo(String moo) {
        this.moo = moo;
    }

    public String getSoi() {
        return soi;
    }

    public void setSoi(String soi) {
        this.soi = soi;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getSubDistrict() {
        return subDistrict;
    }

    public void setSubDistrict(String subDistrict) {
        this.subDistrict = subDistrict;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getIssueDateFormatted() {
        return issueDateFormatted;
    }

    public void setIssueDateFormatted(String issueDateFormatted) {
        this.issueDateFormatted = issueDateFormatted;
    }

    public String getExpireDateFormatted() {
        return expireDateFormatted;
    }

    public void setExpireDateFormatted(String expireDateFormatted) {
        this.expireDateFormatted = expireDateFormatted;
    }
}
