package com.example.acs_cardreader.smc;

import static com.example.acs_cardreader.smc.ThaiADPU.*;

import android.os.Build;
import android.util.Log;

import com.acs.smartcard.Reader;
import com.acs.smartcard.ReaderException;
import com.example.acs_cardreader.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class ThaiSmartCard {
    private static final String TAG = "ThaiSmartCard";
    private Reader reader;
    private SmartCardDevice smartCardDevice;

    public ThaiSmartCard(Reader reader) {
        this.reader = reader;
        smartCardDevice = new SmartCardDevice(reader);
    }

    public Person getInfo() {
        try {
            smartCardDevice.resetCard(reader);
            smartCardDevice.setProtocol(reader);
            smartCardDevice.selectThaiCard();

            String idCard = smartCardDevice.getData(CMD_ID);
            String fullNameTH = smartCardDevice.getData(CMD_TH_FULL_NAME);
            String fullNameEN = smartCardDevice.getData(CMD_EN_FULL_NAME);
            String birthDate = smartCardDevice.getData(CMD_BRITH_DATE);
            String gender = smartCardDevice.getData(CMD_GENDER);
            String issuer = smartCardDevice.getData(CMD_ISSUER);
            String issueDate = smartCardDevice.getData(CMD_ISSUE_DATE);
            String expireDate = smartCardDevice.getData(CMD_EXPIRE_DATE);
            String address = smartCardDevice.getData(CMD_ADDRESS);
            String religion = smartCardDevice.getData(CMD_RELIGION);
            String referenceNo = smartCardDevice.getData(CMD_REFERENCE_NO);
            byte[] photo = smartCardDevice.getPhoto(new byte[][]{
                    CMD_PHOTO1, CMD_PHOTO2, CMD_PHOTO3, CMD_PHOTO4, CMD_PHOTO5,
                    CMD_PHOTO6, CMD_PHOTO7, CMD_PHOTO8, CMD_PHOTO9, CMD_PHOTO10,
                    CMD_PHOTO11, CMD_PHOTO12, CMD_PHOTO13, CMD_PHOTO14, CMD_PHOTO15,
                    CMD_PHOTO16, CMD_PHOTO17, CMD_PHOTO18, CMD_PHOTO19, CMD_PHOTO20});
            smartCardDevice.selectChipCardAMD();
            String laserCode = smartCardDevice.getLaser(CMD_LASER_CODE);

            /* Set to Object */
            Person person = new Person();
            person.setId(idCard);

            String[] fullNameTHArr = fullNameTH.split("#");
            String prefixNameTH = fullNameTHArr[0];
            String firstNameTH = fullNameTHArr[1];
            String middleNameTH = fullNameTHArr[2];
            String lastNameTH = fullNameTHArr[3];

            String fullNameNewTH = "";
            for (String item : fullNameTHArr) {
                if (item != null && !item.isEmpty()) {
                    if (fullNameNewTH.length() > 0) {
                        fullNameNewTH += " ";
                    }
                    fullNameNewTH += item;
                }
            }
            person.setFullNameTH(fullNameNewTH);
            person.setPrefixNameTH(prefixNameTH);
            person.setFirstNameTH(firstNameTH);
            person.setMiddleNameTH(middleNameTH);
            person.setLastNameTH(lastNameTH);

            String[] fullNameENArr = fullNameEN.split("#");
            String prefixNameEN = fullNameENArr[0];
            String firstNameEN = fullNameENArr[1];
            String middleNameEN = fullNameENArr[2];
            String lastNameEN = fullNameENArr[3];
            String fullNameNewEN = "";
            for (String item : fullNameENArr) {
                if (item != null && !item.isEmpty()) {
                    if (fullNameNewEN.length() > 0) {
                        fullNameNewEN += " ";
                    }
                    fullNameNewEN += item;
                }
            }
            person.setFullNameEN(fullNameNewEN);
            person.setPrefixNameEN(prefixNameEN);
            person.setFirstNameEN(firstNameEN);
            person.setMiddleNameEN(middleNameEN);
            person.setLastNameEN(lastNameEN);

            person.setBirthDate(birthDate);
            person.setBirthDateFormatted(dateToThaiFormatted(birthDate));

            person.setGender(genderCodeToString(gender));
            person.setIssuer(issuer);
            person.setIssueDate(issueDate);
            person.setIssueDateFormatted(dateToThaiFormatted(issueDate));
            person.setExpireDate(expireDate);
            person.setExpireDateFormatted(dateToThaiFormatted(expireDate));
            person.setReligion(religionCodeToString(religion));
            person.setReferenceNo(referenceNo);

            String[] addressArr = address.split("#");
            String houseNo = addressArr[0].trim();
            String moo = addressArr[1].trim();
            String soi = addressArr[2].trim();
            String street = addressArr[3].trim();
            String subDistrict = addressArr[5].trim();
            String district = addressArr[6].trim();
            String province = addressArr[7].trim();
            String addressFull = "";
            for (String item : addressArr) {
                if (item != null && !item.isEmpty()) {
                    if (addressFull.length() > 0) {
                        addressFull += " ";
                    }
                    addressFull += item;
                }
            }
            person.setAddress(addressFull);
            person.setHouseNo(houseNo);
            person.setMoo(moo);
            person.setSoi(soi);
            person.setStreet(street);
            person.setSubDistrict(subDistrict);
            person.setDistrict(district);
            person.setProvince(province);

            person.setPhoto(photo);
            person.setLaserCode(laserCode);

            return person;

        } catch (ReaderException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private String dateToThaiFormatted(String date) {
        String dateFormatted = "";
        if (date != null && !date.isEmpty()) {
            String year;
            String month = "01";
            String day = "01";
            year = date.substring(0, 4);
            if (date.length() > 4) {
                month = date.substring(4, 6);
                if (date.length() > 6) {
                    day = date.substring(6, 8);
                }
            }
            Locale localeTH = new Locale("th", "TH");
            Calendar calendar = Calendar.getInstance(localeTH);
            calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(day));
            calendar.set(Calendar.MONTH, Integer.parseInt(month) - 1);
            calendar.set(Calendar.YEAR, Integer.parseInt(year));
            SimpleDateFormat thaiFormatter = new SimpleDateFormat("dd MMM yyyy", localeTH);
            dateFormatted = thaiFormatter.format(calendar.getTime());
        }
        return dateFormatted;
    }

    private String genderCodeToString(String code) {
        if (code.equals("") || code.isEmpty()) {
            return "";
        }
        String result = "";
        switch (code) {
            case "1":
                result = "Male";
                break;
            case "2":
                result = "Female";
                break;
            default:
                result = "";
        }

        return result;
    }

    private String religionCodeToString(String code) {
        if (code.equals("") || code.isEmpty()) {
            return "";
        }
        String result = "";
        switch (Integer.parseInt(code)) {
            case 1:
                result = "พุทธ";
                break;
            case 2:
                result = "อิสลาม";
                break;
            case 3:
                result = "คริสต์";
                break;
            case 4:
                result = "พราหมณ์-ฮินดู";
                break;
            case 5:
                result = "ซิกข์";
                break;
            case 6:
                result = "ยิว";
                break;
            case 7:
                result = "เชน";
                break;
            case 8:
                result = "โซโรอัสเตอร์";
                break;
            case 9:
                result = "บาไฮ";
                break;
            case 0:
                result = "ไม่นับถือศาสนา";
                break;
            default:
                result = "";
        }

        return result;
    }

}
