package com.example.acs_cardreader.smc;

import android.util.Log;

import com.acs.smartcard.Reader;
import com.acs.smartcard.ReaderException;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;

public class SmartCardDevice {
    private Reader reader;
    private int slotNum = 0;

    public SmartCardDevice(Reader reader) {
        this.reader = reader;
    }

    public byte[] resetCard(Reader r) throws ReaderException {
        return r.power(0, Reader.CARD_WARM_RESET);
    }

    public int setProtocol(Reader r) throws ReaderException {
        return r.setProtocol(0, Reader.PROTOCOL_T0);
    }

    public int selectThaiCard() throws ReaderException {
        byte[] response = new byte[500];
        byte[] selectCard = new byte[ThaiADPU.SELECT.length + ThaiADPU.THAI_CARD.length];
        System.arraycopy(ThaiADPU.SELECT, 0, selectCard, 0, ThaiADPU.SELECT.length);
        System.arraycopy(ThaiADPU.THAI_CARD, 0, selectCard, ThaiADPU.SELECT.length, ThaiADPU.THAI_CARD.length);

        return reader.transmit(0, selectCard, selectCard.length, response, response.length);
    }

    public int selectChipCardAMD() throws ReaderException {
        byte[] response = new byte[500];
        byte[] selectCard = new byte[ThaiADPU.SELECT.length + ThaiADPU.CHIP_CARD_AMD.length];
        System.arraycopy(ThaiADPU.SELECT, 0, selectCard, 0, ThaiADPU.SELECT.length);
        System.arraycopy(ThaiADPU.CHIP_CARD_AMD, 0, selectCard, ThaiADPU.SELECT.length, ThaiADPU.CHIP_CARD_AMD.length);

        return reader.transmit(0, selectCard, selectCard.length, response, response.length);
    }


    public String getData(byte[] command) throws ReaderException {
        byte[] response = new byte[500];
        // send command
        reader.transmit(slotNum, command, command.length, response, response.length);
        // get response
        byte[] responseCMD = new byte[ThaiADPU.RESPONSE.length + 1];

        System.arraycopy(ThaiADPU.RESPONSE, 0, responseCMD, 0, ThaiADPU.RESPONSE.length);
        byte[] lastByte = new byte[1];
        System.arraycopy(command, command.length - 1, lastByte, 0, 1);
        System.arraycopy(lastByte, 0, responseCMD, ThaiADPU.RESPONSE.length, lastByte.length);

        int responseLength = reader.transmit(slotNum, responseCMD, responseCMD.length, response, response.length);
        String data = byteArrayToHexString(response, 0, responseLength);

        return data;
    }

    public String getLaser(byte[] command) throws ReaderException {
        byte[] response = new byte[500];
        // send command
        reader.transmit(slotNum, command, command.length, response, response.length);
        // get response
        byte[] responseCMD = new byte[ThaiADPU.RESPONSE.length + 1];
        System.arraycopy(ThaiADPU.RESPONSE, 0, responseCMD, 0, ThaiADPU.RESPONSE.length);
        byte[] lastByte = new byte[]{(byte) 0x10};
        System.arraycopy(lastByte, 0, responseCMD, ThaiADPU.RESPONSE.length, lastByte.length);

        int responseLength = reader.transmit(slotNum, responseCMD, responseCMD.length, response, response.length);
        String data = byteArrayToHexString(response, 0, responseLength);

        return data;
    }

    public byte[] getPhoto(byte[][] command) throws ReaderException {
        byte[] response = new byte[500];
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        byte[] responseCMD = new byte[ThaiADPU.RESPONSE.length + 1];
        System.arraycopy(ThaiADPU.RESPONSE, 0, responseCMD, 0, ThaiADPU.RESPONSE.length);
        byte[] lastByte = new byte[1];
        System.arraycopy(command[0], command[0].length - 1, lastByte, 0, 1);
        System.arraycopy(lastByte, 0, responseCMD, ThaiADPU.RESPONSE.length, lastByte.length);

        for (int i = 0; i < command.length; i++) {
            reader.transmit(slotNum, command[i], command[i].length, response, response.length);
            int responseLength = reader.transmit(slotNum, responseCMD, responseCMD.length, response, response.length);
            buffer.write(response, 0, responseLength - 2);
        }
        byte[] photoBuffer = buffer.toByteArray();

        return photoBuffer;
    }

    private String byteArrayToHexString(byte[] input, int index, int responseLength) {
        int length = responseLength;
        if (length + index > input.length) {
            length = input.length - index;
        }
        byte[] selectBytes = new byte[length];
        System.arraycopy(input, index, selectBytes, 0, length - 2);
        return showByteString(selectBytes);
    }

    public String showByteString(byte[] input) {
        StringBuilder output;
        if (input == null) {
            Log.d("TAG", "Empty");
        }
        output = new StringBuilder();
        for (byte b : input) {

            output.append(String.format("%02x", b));
        }
        Log.d("TAG", "Byte String - " + output.toString());

        String result = null;
        try {
            result = new String(input, "TIS620");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            Log.d("TAG", "Cannot parse!");
        }
        return result.trim();
    }
}
