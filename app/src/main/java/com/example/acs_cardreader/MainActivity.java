package com.example.acs_cardreader;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.acs.smartcard.Reader;
import com.acs.smartcard.ReaderException;
import com.example.acs_cardreader.reader.ThaiReader;
import com.example.acs_cardreader.smc.Person;
import com.example.acs_cardreader.smc.ThaiSmartCard;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private static final String ACTION_USB_PERMISSION = "com.example.acs_cardreader.USB_PERMISSION";
    private Button buttonFind, buttonOpen, buttonPower, buttonRead, buttonClose;
    private TextView textMessage;
    private ImageView imageView;

    UsbDevice usbDevice = null;
    private Reader reader;
    private ThaiReader thaiReader;
    private int slotNum = 0;
    private PendingIntent mPermissionIntent;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initReader();
        initView();
        initListener();
    }

    private void initReader() {
        UsbManager usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        reader = new Reader(usbManager);
        thaiReader = new ThaiReader();
    }

    private void initView() {
        buttonFind = findViewById(R.id.buttonFind);
        buttonOpen = findViewById(R.id.buttonOpen);
        buttonPower = findViewById(R.id.buttonPower);
        buttonRead = findViewById(R.id.buttonRead);
        buttonClose = findViewById(R.id.buttonClose);
        textMessage = findViewById(R.id.textMessage);
        imageView = findViewById(R.id.imageView);
    }

    private void initListener() {
        buttonFind.setOnClickListener(view -> {
            findDevice();
        });
        buttonOpen.setOnClickListener(view -> {
            open();
        });
        buttonPower.setOnClickListener(view -> {
            powerOnCard();
        });
        buttonRead.setOnClickListener(view -> {
            read2();
        });
        buttonClose.setOnClickListener(view -> {
            close();
        });

        reader.setOnStateChangeListener((_slotNum, _prevState, _currState) -> {
            Log.d("TAG", "State -> change listener");
            Log.d("TAG", "slotNum -> " + _slotNum);
            Log.d("TAG", "prevState -> " + _prevState);
            Log.d("TAG", "currState -> " + _currState);
            if (_prevState == 1 && _currState == 2) {
                Log.d("TAG", "state - card inserted");
                setText("state - card inserted");
            }
            if (_prevState == 2 && _currState == 1) {
                Log.d("TAG", "state - card removed");
                setText("state - card removed");
            }
            slotNum = _slotNum;
        });
    }

    private void findDevice() {
        UsbManager usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        for (UsbDevice device : usbManager.getDeviceList().values()) {
            Log.d("TAG", "Found - " + device.getVendorId() + " " + device.getProductId() + " " + device.getDeviceName());
            setText("Found - " + device.getVendorId() + " " + device.getProductId() + " " + device.getDeviceName());
            //            if (device.getVendorId() == 1839 && device.getProductId() == 45312) {
            usbDevice = device;
            break;
//            }
        }
        if (usbDevice == null) return;
        mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_USB_PERMISSION);
        registerReceiver(mUsbPermissionReceiver, filter);

        UsbManager manager = (UsbManager) getSystemService(Context.USB_SERVICE);
        if (manager != null) {
            Log.d("MainActivity", "Start request permission");
            manager.requestPermission(usbDevice, mPermissionIntent);
        } else {
            throw new RuntimeException("USB manager not found");
        }
    }

    private void open() {
        try {
            reader.open(usbDevice);
            Log.d("TAG", "Connect - " + usbDevice.getDeviceName());
            setText("Connect - " + usbDevice.getDeviceName());
            setText("Connect - please insert card");
        } catch (IllegalArgumentException e) {
            Log.e("TAG", e.getMessage());
            setText(e.getMessage());
        }
    }

    private void powerOnCard() {
        // power on card
        try {
            byte[] atr = thaiReader.resetCard(reader);
            Log.d("TAG", "Power on");
            setText("Power on");
            setText("ATR: " + thaiReader.showByteString(atr));
        } catch (ReaderException e) {
            Log.d("TAG", "Power error");
            setText(e.getMessage());
        }

        // set state
        try {
            int protocol = thaiReader.setProtocol(reader);
            Log.d("TAG", "Set protocol - " + protocol);
            setText("Set protocol - " + protocol);
        } catch (ReaderException e) {
            Log.d("TAG", "Set protocol - Error");
            setText(e.getMessage());
        }

        try {
            // get id card info
            int responseLength;
            responseLength = thaiReader.select(reader);
            Log.d("TAG", "Response byte - " + responseLength);
            setText("Select Card");
        } catch (ReaderException e) {
            Log.d("TAG", "Error");
            setText(e.getMessage());
        }
    }

    private void read2() {
        ThaiSmartCard thaiSmartCard = new ThaiSmartCard(reader);
        Person person = thaiSmartCard.getInfo();
        setText(person.toString());
        Bitmap bitmap = BitmapFactory.decodeByteArray(person.getPhoto(), 0, person.getPhoto().length);
        imageView.setImageBitmap(bitmap);
    }

    private void read() {
        try {
            HashMap<String, Object> response = new HashMap<>();
            response = thaiReader.readAll(reader);
            for (Map.Entry<String, Object> set : response.entrySet()) {
                setText(set.getKey() + ": " + set.getValue());
                if (set.getKey().equals("photo")) {
                    byte[] pictureBuffer;
                    pictureBuffer = (byte[]) set.getValue();
                    Bitmap bitmap = BitmapFactory.decodeByteArray(pictureBuffer, 0, pictureBuffer.length);
                    imageView.setImageBitmap(bitmap);
                } else if (set.getKey().equals("address")) {
                    String address = (String) set.getValue();
                    String addrHouseNo = address.split("#")[0].trim();
                    String addrVillageNo = address.split("#")[1].trim();
                    String addrLane = address.split("#")[2].trim();
                    String addrRoad = address.split("#")[3].trim();
                    String addrTambol = address.split("#")[5].trim();
                    String addrAmphur = address.split("#")[6].trim();
                    String addrProvince = address.split("#")[7].trim();
                }
            }
        } catch (ReaderException e) {
            Log.d("TAG", "Read Error");
            setText(e.getMessage());
        }
    }

    private void close() {
        setText("Close");
        reader.close();
    }

    void setText(String text) {
        TextView textMessage = findViewById(R.id.textMessage);
        String result = textMessage.getText().toString() + "\n" + text;
        runOnUiThread(() -> textMessage.setText(result));
    }

    private final BroadcastReceiver mUsbPermissionReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            UsbManager manager;
            if (ACTION_USB_PERMISSION.equals(action)) {
                Log.d("Main Activity", "USB permission broadcast received");
                synchronized (this) {
                    UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

                    if (device != null && usbDevice != null && device.getDeviceName().equals(usbDevice.getDeviceName())) {
                        if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                            manager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
                            if (manager == null) {
                                throw new RuntimeException("USB manager not found");
                            }
                            open();
                        } else {
                            throw new RuntimeException("Device is not granted");
                        }

                    }
                }
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mUsbPermissionReceiver != null)
            unregisterReceiver(mUsbPermissionReceiver);
    }
}
